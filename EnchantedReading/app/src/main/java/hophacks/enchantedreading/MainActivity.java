package hophacks.enchantedreading;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hophacks.enchantedreading.activities.DifficultWordsActivity;
import hophacks.enchantedreading.activities.StoryActivity;
import hophacks.enchantedreading.adapters.StoryListAdapter;
import hophacks.enchantedreading.obj.Story;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        populateStoryList();
        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Rumpelstiltskin.ttf");
        ((TextView)findViewById(R.id.stripTitle)).setTypeface(tf);
        TextView difficultWords = (TextView) findViewById(R.id.difficultWords);
        difficultWords.setTypeface(tf);
        difficultWords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDifficultWords();
            }
        });
    }

    private void populateStoryList() {
        ListView storyListView = (ListView) findViewById(R.id.storyList);
        List<Story> storyList = getStories();
        ArrayAdapter stories = new ArrayAdapter(this, android.R.layout.simple_list_item_2, storyList);
        storyListView.setAdapter(stories);

        StoryListAdapter adapter = new StoryListAdapter(this, storyList);
        storyListView.setAdapter(adapter);

        storyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Story story = (Story) parent.getItemAtPosition(position);
                getStory(story);
            }
        });
    }

    @NonNull
    private List<Story> getStories() {
        List<Story> storyList = new ArrayList<>();
        Story story = new Story();
        story.setTitle("The Clever Fox");
        story.setFilepath("clever_fox");
        story.setImage(R.drawable.clever_fox);
        storyList.add(story);

        Story story9 = new Story();
        story9.setTitle("Rapunzel");
        story9.setFilepath("rapunzel");
        story9.setImage(R.drawable.rapunzel);
        storyList.add(story9);
/**
        Story story2 = new Story();
        story2.setTitle("Three Little Pigs");
        story2.setFilepath("three_little_pigs");
        story2.setImage(R.drawable.three_little_pigs);
        storyList.add(story2);
**/
        Story story3 = new Story();
        story3.setTitle("Two Goats");
        story3.setFilepath("two_goats");
        story3.setImage(R.drawable.two_goats);
        storyList.add(story3);

        Story story1 = new Story();
        story1.setTitle("A House for Me");
        story1.setFilepath("house_for_me");
        story1.setImage(R.drawable.house_for_me);
        storyList.add(story1);

        Story story4 = new Story();
        story4.setTitle("Jack and Jill");
        story4.setFilepath("jack");
        story4.setImage(R.drawable.jack);
        storyList.add(story4);
        
        return storyList;
    }


    public void getStory(Story story) {
        Intent intent = new Intent(this, StoryActivity.class);
        intent.putExtra(StoryActivity.TITLE, story.getTitle());
        intent.putExtra(StoryActivity.FILE, story.getFilepath());
        startActivity(intent);
    }


    public void getDifficultWords() {
        Intent intent = new Intent(this, DifficultWordsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_difficult_words){
            getDifficultWords();
        }

        return super.onOptionsItemSelected(item);
    }
}
