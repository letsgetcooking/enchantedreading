package hophacks.enchantedreading.obj;

import java.util.List;
import java.util.Map;

public class Story {

    private String filepath;
    private String title;
    private Map<String, String> imageDictionary;
    private List<String> lines;
    private int level;
    private int image;

    public Story() {
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String file) {
        this.filepath = file;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, String> getImageDictionary() {
        return imageDictionary;
    }

    public void setImageDictionary(Map<String, String> imageDictionary) {
        this.imageDictionary = imageDictionary;
    }

    public List<String> getLines() {
        return lines;
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    public int getLevel() {
        return level;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
