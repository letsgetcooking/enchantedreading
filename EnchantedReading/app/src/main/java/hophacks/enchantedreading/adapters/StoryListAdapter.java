package hophacks.enchantedreading.adapters;


import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hophacks.enchantedreading.R;
import hophacks.enchantedreading.obj.Story;

public class StoryListAdapter extends ArrayAdapter<Story> {
    private final Activity context;
    private final List<Story> stories;

    public StoryListAdapter(Activity context, List<Story> stories) {
        super(context, R.layout.story_list, stories);

        this.context = context;
        this.stories = stories;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.story_list, null, true);


        Typeface tf = Typeface.createFromAsset(context.getAssets(),"fonts/Rumpelstiltskin.ttf");

        TextView txtTitle = (TextView) rowView.findViewById(R.id.title);
        txtTitle.setTypeface(tf);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        txtTitle.setText(stories.get(position).getTitle());
        imageView.setImageResource(stories.get(position).getImage());
        return rowView;
    }

    ;
}
