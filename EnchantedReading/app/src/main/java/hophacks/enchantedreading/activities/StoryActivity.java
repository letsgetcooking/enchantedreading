package hophacks.enchantedreading.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import hophacks.enchantedreading.R;
import hophacks.enchantedreading.obj.Story;

public class StoryActivity extends Activity {

    private SpeechRecognizer mSpeechRecognizer;
    private StoryListener listener;

    public static final String TITLE = "title";
    public static final String FILE = "filepath";
    private Story story;
    private SpannableStringBuilder spantext;
    private int lineNum;
    private List<Integer> lineOffsets;
    private int totalWords;
    private int correctWords;
    public static final String[] SET_VALUES = new String[]{"goat", "bridge", "crow", "fox", "pig", "wolf", "house", "hill", "water", "pail"};
    public static final Set<String> MY_SET = new HashSet<String>(Arrays.asList(SET_VALUES));
    public TextToSpeech t1;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSpeechRecognizer.destroy();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_story);
        lineOffsets = new ArrayList<>();
        Intent intent = getIntent();
        String title = intent.getStringExtra(TITLE);
        String filepath = intent.getStringExtra(FILE);
        story = new Story();
        story.setTitle(title);
        story.setFilepath(filepath);
        SpannableStringBuilder sb = setStoryLines(filepath);
        buildLineOffsets(sb);
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        listener = new StoryListener(this);

        displayStory(sb, title);
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });


        Typeface tfR = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Rumpelstiltskin.ttf");
        Typeface tfK = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/KGCorneroftheSky.ttf");
        ((TextView)findViewById(R.id.storyTitle)).setTypeface(tfR);
        ((TextView)findViewById(R.id.storyText)).setTypeface(tfK);

        lineNum = 0;
        startReading();
    }

    private SpannableStringBuilder setStoryLines(String filepath) {
        SpannableStringBuilder sb = new SpannableStringBuilder();
        InputStream ins = getApplicationContext().getResources().openRawResource(
                this.getApplicationContext().getResources().getIdentifier(filepath,
                        "raw", getApplicationContext().getPackageName()));
        InputStreamReader inputReader = new InputStreamReader(ins);
        BufferedReader buffReader = new BufferedReader(inputReader);
        String line;
        List<String> lines = new ArrayList<>();
        int start = 0;
        int end;
        int curr_index = 0;

        try {
            int offset = 0;
            while ((line = buffReader.readLine()) != null) {
                sb.append(line).append("\n");

                //if (offset == 0)
                for (String word : line.split(" ")) {
                    if (MY_SET.contains(word)) {
                        Log.w("start", Integer.toString(start));
                        end = start + word.length();
                        curr_index = end+1;
                        Log.w("end", Integer.toString(end));
                        Log.w("word", word);
                        sb.setSpan(new CustomClickableSpan(word), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else {
                        start = curr_index + word.length() + 1;
                        curr_index = start;
                    }
                }
                start++;
                List<String> myList = new ArrayList<>(Arrays.asList(line.split("\\.")));
                for (String s : myList) {
                    String s2 = s.replaceAll("[^a-zA-Z0-9\\s]", "").trim();
                    if (!s2.isEmpty()) {
                        lineOffsets.add(offset);
                        lines.add(s2);
                    }
                    offset += s.length() + 1;
                }
                offset++;
            }
            lineOffsets.add(offset);
        } catch (IOException e) {
        }
        story.setLines(lines);
        return sb;
    }

    public void buildLineOffsets(SpannableStringBuilder sb) {
        String line = sb.toString();
        lineOffsets = new ArrayList<>();
        lineOffsets.add(0);
        int offset = 0;
        while (offset < line.length()) {
            if (line.charAt(offset) == '.') {
                lineOffsets.add(offset + 1);
            }
            offset++;
        }
    }

    public void startReading() {
        promptSpeechInput();
    }

    public void nextLine(String speech) {
        if (lineOffsets.get(lineNum + 1) - 1 >= spantext.length()) {
            return;
        }

        TextView storytext = (TextView) findViewById(R.id.storyText);

        String line = spantext.toString().substring(lineOffsets.get(lineNum), lineOffsets.get(lineNum + 1));
        Log.e("tag", line);
        String[] words = line.split(" ");
        int offset = lineOffsets.get(lineNum);
        int i = 0;
        while (i < line.length() && offset < lineOffsets.get(lineNum + 1)) {
            if (line.charAt(i) == ' ') {
                i++;
            } else {
                String word = "" + line.charAt(i);
                i++;
                while (i < line.length() && line.charAt(i) != ' ') {
                    word += line.charAt(i);
                    i++;
                }
                String compareWord = word.toLowerCase().replaceAll("[^a-zA-Z0-9\\s]", "").trim();
                ForegroundColorSpan redfont = new ForegroundColorSpan(Color.parseColor("#a94b0b"));
                ForegroundColorSpan greenfont = new ForegroundColorSpan(Color.parseColor("#049615"));
                if (speech.contains(compareWord)) {
                    spantext.setSpan(greenfont, offset + i - word.length(), offset + i, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                    storytext.setText(spantext);
                    correctWords++;
                } else {
                    spantext.setSpan(redfont, offset + i - word.length(), offset + i, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                    storytext.setText(spantext);
                }
                totalWords++;
            }
        }

        lineNum++;
        if (lineNum >= story.getLines().size()) {
            int score = correctWords*100/totalWords;

            final Dialog dialog = new Dialog(StoryActivity.this);
            dialog.setContentView(R.layout.popup2);
            dialog.setTitle("Score: "+score);
            //dialog.setText("Score: "+score);

            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            Resources res = getResources();
            String mDrawableName = "stars3";
            if(score<90){
                mDrawableName = "stars2";
            }if(score <70){
                mDrawableName = "stars1";
            }
            int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
            Drawable drawable = res.getDrawable(resID );
            image.setImageDrawable(drawable );

            TextView textScore = (TextView) dialog.findViewById(android.R.id.title);
            Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Rumpelstiltskin.ttf");
            textScore.setTypeface(tf);
            textScore.setText("Score: "+score);
            textScore.setTextSize(30);
            textScore.setGravity(Gravity.CENTER);
//            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
//            dialogButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//                }
//            });

            dialog.show();


        } else {
            promptSpeechInput();
        }
    }

    public void readAgain() {
     //   TextView text = (TextView) findViewById(R.id.speechText);
        //   text.setText("read again " + lineNum);
        promptSpeechInput();
    }

    public void displayStory(SpannableStringBuilder sb, String curtitle) {

        TextView title = (TextView) findViewById(R.id.storyTitle);
        if (curtitle != null) {
            title.setText(curtitle);
        }
        TextView text = (TextView) findViewById(R.id.storyText);
        text.setMovementMethod(LinkMovementMethod.getInstance());
        spantext = sb;
        text.setText(spantext);
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 100);

        mSpeechRecognizer.setRecognitionListener(listener);
        mSpeechRecognizer.startListening(intent);
    }

    private void promptSpeechInput2() {
        Random rand = new Random();
        if (rand.nextBoolean()) {
            nextLine(story.getLines().get(lineNum));
        } else {
            nextLine("A string");
        }

    }


    public void getDifficultWords() {
        Intent intent = new Intent(this, DifficultWordsActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_story, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_difficult_words) {
            getDifficultWords();
        }

        return super.onOptionsItemSelected(item);
    }

    class StoryListener implements RecognitionListener {
        private StoryActivity activity;

        public StoryListener(StoryActivity activity) {
            this.activity = activity;
        }

        public void onReadyForSpeech(Bundle params) {
            //   Log.d(TAG, "onReadyForSpeech");
        }

        public void onBeginningOfSpeech() {
            //   Log.d(TAG, "onBeginningOfSpeech");
        }

        public void onRmsChanged(float rmsdB) {
            //   Log.d(TAG, "onRmsChanged");
        }

        public void onBufferReceived(byte[] buffer) {
            //   Log.d(TAG, "onBufferReceived");
        }

        public void onEndOfSpeech() {
            //   Log.d(TAG, "onEndofSpeech");
        }

        public void onError(int error) {
            activity.readAgain();
        }

        public void onResults(Bundle results) {
            ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            activity.nextLine(data.get(0));
        }

        public void onPartialResults(Bundle partialResults) {
            //ArrayList<String> data = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            //text.setText(text.getText() + data.get(0));
        }

        public void onEvent(int eventType, Bundle params) {
            //  Log.d(TAG, "onEvent " + eventType);
        }
    }

    private class CustomClickableSpan extends ClickableSpan {
        private final String word;

        public CustomClickableSpan(String word) {
            this.word = word;
        }

        @Override
        public void onClick(View view) {
            Log.w("tag", word);
            final Dialog dialog = new Dialog(StoryActivity.this);
            dialog.setContentView(R.layout.popup);
            //dialog.setTitle(word);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            CharSequence cs = word;
            t1.speak(cs, TextToSpeech.QUEUE_FLUSH, null, "1");
            TextView text = (TextView) dialog.findViewById(R.id.text);
            text.setText(word);
            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            Resources res = getResources();
            String mDrawableName = word;
            int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
            Drawable drawable = res.getDrawable(resID );
            image.setImageDrawable(drawable );

//            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
//            dialogButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//                }
//            });

            dialog.show();
        }
    }
}
