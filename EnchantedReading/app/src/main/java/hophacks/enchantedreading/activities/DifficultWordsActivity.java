package hophacks.enchantedreading.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import hophacks.enchantedreading.R;

public class DifficultWordsActivity extends Activity {


    private SpeechRecognizer mSpeechRecognizer;
    private DifficultWordListener listener;

    private String currentWord;

    private Set<String> words;
    private int wrongAnswers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_difficult_words);


        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        listener = new DifficultWordListener(this);

        words = new HashSet<>();
        words.add("house");
        words.add("horse");
        words.add("castle");
        words.add("goat");
        words.add("mouse");
        words.add("hummus");
        words.add("crow");
        words.add("bridge");
        words.add("fox");
        words.add("wolf");
        words.add("water");
        words.add("hill");


        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),"fonts/Rumpelstiltskin.ttf");
        ((TextView)findViewById(R.id.difficultWord)).setTypeface(tf);
        ((TextView)findViewById(R.id.difficultWordFeedback)).setTypeface(tf);

        ImageView tryAgainB = (ImageView) findViewById(R.id.difficultWordTryAgainButton);
        ImageView nextWordB = (ImageView) findViewById(R.id.difficultWordNextWordButton);

        Resources res = getResources(); /** from an Activity */
        tryAgainB.setImageDrawable(res.getDrawable(R.drawable.try_again));
        nextWordB.setImageDrawable(res.getDrawable(R.drawable.next_word));
        tryAgainB.setVisibility(View.GONE);
        nextWordB.setVisibility(View.GONE);

        nextWord();
    }

    public void nextWord(){
        wrongAnswers=0;
        int size = words.size();
        int item = new Random().nextInt(size); // In real life, the Random object should be rather more shared than this
        int i = 0;
        for(String w : words)
        {
            if (i == item)
                currentWord = w;
            i = i + 1;
        }

        TextView wordView = (TextView)findViewById(R.id.difficultWord);
        wordView.setText(currentWord);

        ImageView imageView = (ImageView) findViewById(R.id.wordImage);
        Resources res = getResources();
        String mDrawableName = currentWord;
        int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
        Drawable drawable = res.getDrawable(resID );
        imageView.setImageDrawable(drawable );

        ImageView tryAgainB = (ImageView) findViewById(R.id.difficultWordTryAgainButton);
        ImageView nextWordB = (ImageView) findViewById(R.id.difficultWordNextWordButton);

        tryAgainB.setVisibility(View.GONE);
        nextWordB.setVisibility(View.GONE);

        TextView feedback = (TextView)findViewById(R.id.difficultWordFeedback);
        feedback.setText("");

        promptSpeechInput();
    }

    public void promptSpeechInput(){

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS , 300);

        mSpeechRecognizer.setRecognitionListener(listener);
        mSpeechRecognizer.startListening(intent);
    }


    private void promptSpeechInput2(){
        Random rand = new Random();
        if (rand.nextBoolean()) {
            giveAnswer(currentWord);
        }
        else{
            giveAnswer("something else");
        }
    }

    public void tryAgain(){
        ImageView tryAgainB = (ImageView) findViewById(R.id.difficultWordTryAgainButton);
        tryAgainB.setVisibility(View.GONE);
        promptSpeechInput();
    }

    public void tryAgain(View v){
        tryAgain();
    }

    public void nextWord(View v){
        nextWord();
    }

    public void giveAnswer(String answer){
        ImageView tryAgainB = (ImageView) findViewById(R.id.difficultWordTryAgainButton);
        ImageView nextWordB = (ImageView) findViewById(R.id.difficultWordNextWordButton);

        if(answer.compareToIgnoreCase(currentWord)==0){
            tryAgainB.setVisibility(View.GONE);
            nextWordB.setVisibility(View.VISIBLE);
            TextView wordView = (TextView)findViewById(R.id.difficultWordFeedback);
            wordView.setText("Correct!");
        }else{
            wrongAnswers++;
            tryAgainB.setVisibility(View.VISIBLE);
            nextWordB.setVisibility(View.GONE);
            TextView wordView = (TextView)findViewById(R.id.difficultWordFeedback);
            wordView.setText("No, not "+ answer+".");
            if(wrongAnswers>3){
                nextWordB.setVisibility(View.VISIBLE);
            }
        }
    }


    class DifficultWordListener implements RecognitionListener
    {
        private DifficultWordsActivity activity;
        public DifficultWordListener( DifficultWordsActivity activity){
            this.activity = activity;
        }
        public void onReadyForSpeech(Bundle params)
        {
            //   Log.d(TAG, "onReadyForSpeech");
        }
        public void onBeginningOfSpeech()
        {
            //   Log.d(TAG, "onBeginningOfSpeech");
        }
        public void onRmsChanged(float rmsdB)
        {
            //   Log.d(TAG, "onRmsChanged");
        }
        public void onBufferReceived(byte[] buffer)
        {
            //   Log.d(TAG, "onBufferReceived");
        }
        public void onEndOfSpeech()
        {
            //   Log.d(TAG, "onEndofSpeech");
        }
        public void onError(int error)
        {
           // activity.giveAnswer("Error "+error);
        }
        public void onResults(Bundle results)
        {
            ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            activity.giveAnswer(data.get(0));
        }
        public void onPartialResults(Bundle partialResults)
        {
            //ArrayList<String> data = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            //text.setText(text.getText() + data.get(0));
        }
        public void onEvent(int eventType, Bundle params)
        {
            //  Log.d(TAG, "onEvent " + eventType);
        }
    }

}
